package com.epam.view;

import com.epam.controller.ParseController;
import com.epam.controller.SortController;

import java.util.*;

public class Menu {
    private String name;

    private String text;

    private LinkedHashMap<String, Runnable> actionsMap = new LinkedHashMap<>();

    private ParseController parseController;

    private SortController sortController;

    private Menu(final String name, final String text) {
        this.name = name;
        this.text = text;
    }

    public Menu(ParseController parseController,
                SortController sortController) {
        this.parseController = parseController;
        this.sortController = sortController;
        Menu menu = new Menu("Main menu", "");

        menu.putAction("Print json data", parseController::printParsedJson);
        menu.putAction("Print sorted data", sortController::printSortedData);
        menu.putAction("Exit", () -> System.exit(0));
        activateMenu(menu);
    }


    private void activateMenu(final Menu menu) {
        System.out.println(menu.generateMenu());
        Scanner scanner = new Scanner(System.in);
        while (true) {
            int actionNumber = scanner.nextInt();
            menu.executeAction(actionNumber);
        }
    }

    private void putAction(final String name, final Runnable action) {
        actionsMap.put(name, action);
    }

    private String generateMenu() {
        StringBuilder sb = new StringBuilder();
        sb.append(name).append(" ");
        sb.append(text).append(":\n");
        List<String> actionNames = new ArrayList<>(actionsMap.keySet());
        for (int i = 0; i < actionNames.size(); i++) {
            sb.append(String.format(" %d: %s%n", i + 1, actionNames.get(i)));
        }
        return sb.toString();
    }

    private void executeAction(int actionNumber) {
        actionNumber -= 1;
        if (actionNumber < 0 || actionNumber >= actionsMap.size()) {
            System.out.println("Wrong menu option: " + actionNumber);
        } else {
            List<Runnable> actions = new ArrayList<>(actionsMap.values());
            actions.get(actionNumber).run();
        }
    }
}

