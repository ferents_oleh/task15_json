package com.epam.controller.impl;

import com.epam.controller.ParseController;
import com.epam.service.gson.GsonParser;
import com.epam.service.jackson.JacksonParser;
import com.epam.validation.JsonValidator;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;

import java.io.IOException;
import java.util.Arrays;

public class ParseControllerImpl implements ParseController {
    private GsonParser gsonParser = new GsonParser();

    private JacksonParser jacksonParser = new JacksonParser();

    private String jsonFilePath = "/home/nomorethrow/IdeaProjects/task15_json/src/main/resources/orangery.json";
    private String jsonSchemaPath = "/home/nomorethrow/IdeaProjects/task15_json/src/main/resources/orangerySchema.json";

    @Override
    public void printParsedJson() {
        try {
            if (!validateJson()) {
                System.out.println("Invalid json file");
            } else {
                System.out.println("Gson parser:");
                Arrays.asList(gsonParser.parseFlowers(jsonFilePath))
                        .forEach(System.out::println);
                System.out.println("Jackson parser:");
                Arrays.asList(jacksonParser.parseFlowers(jsonFilePath))
                        .forEach(System.out::println);
            }
        } catch (IOException | ProcessingException e) {
            e.printStackTrace();
        }
    }

    private boolean validateJson() throws IOException, ProcessingException {

        return JsonValidator.isValid(jsonFilePath, jsonSchemaPath);
    }
}
