package com.epam.controller.impl;

import com.epam.controller.SortController;
import com.epam.model.Flower;
import com.epam.service.comparator.FlowerComparator;
import com.epam.service.jackson.JacksonParser;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class SortControllerImpl implements SortController {
    @Override
    public void printSortedData() {
        String jsonFilePath = "/home/nomorethrow/IdeaProjects/task15_json/src/main/resources/orangery.json";
        JacksonParser parser = new JacksonParser();
        try {
            List<Flower> flowers = Arrays.asList(parser.parseFlowers(jsonFilePath));
            flowers.sort(new FlowerComparator());
            flowers.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
