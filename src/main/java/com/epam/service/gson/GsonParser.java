package com.epam.service.gson;

import com.epam.model.Flower;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.*;


public class GsonParser {
    private Gson gson = new Gson();

    public Flower[] parseFlowers(String jsonFilepath) throws FileNotFoundException {
        JsonReader reader = new JsonReader(
                new InputStreamReader(
                        new FileInputStream(jsonFilepath)
                )
        );
        reader.setLenient(true);
        return gson.fromJson(reader, Flower[].class);
    }
}
