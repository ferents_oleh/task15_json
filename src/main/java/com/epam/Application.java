package com.epam;

import com.epam.controller.impl.ParseControllerImpl;
import com.epam.controller.impl.SortControllerImpl;
import com.epam.view.Menu;

public class Application {
    public static void main(String[] args) {
        new Menu(
                new ParseControllerImpl(),
                new SortControllerImpl()
        );
    }
}
