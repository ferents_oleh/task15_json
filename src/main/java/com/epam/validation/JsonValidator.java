package com.epam.validation;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.main.JsonSchema;
import com.github.fge.jsonschema.main.JsonSchemaFactory;

import java.io.File;
import java.io.IOException;

public class JsonValidator {
    public static boolean isValid(String jsonFilepath, String jsonSchemaFilepath) throws IOException, ProcessingException {
        JsonNode schemaNode = JsonLoader.fromFile(new File(jsonSchemaFilepath));
        JsonNode jsonNode = JsonLoader.fromFile(new File(jsonFilepath));
        JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
        JsonSchema schema = factory.getJsonSchema(schemaNode);
        return schema.validInstance(jsonNode);
    }
}
